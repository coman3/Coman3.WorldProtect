﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using TDSM.API;
using TDSM.API.Command;
using TDSM.API.Logging;
using TDSM.API.Misc;
using TDSM.API.Plugin;
using Terraria;

namespace Coman3.WorldProtect
{
    public class WorldProtect : BasePlugin
    {
        public DataRegister DataRegister { get; set; }
        public Dictionary<string, PlayerRegion> Regions { get; set; }

        public WorldProtect()
        {
            TDSMBuild = 5;
            Version = "1";
            Author = "Coman3";
            Name = "Simple World Protect";
            Description = "This plugin protects a specified section of world per player.";
        }

        protected override void Initialized(object state)
        {
            Regions = new Dictionary<string, PlayerRegion>(Tools.MaxPlayers);
            AddCommand("r")
                .WithDescription("Set region point")
                .WithHelpText("r <1|2> <block>")
                .Calls(RegionCommandHandle);
            //DataRegister = new DataRegister(System.IO.Path.Combine(Globals.DataPath, "regions.txt"));
        }

        private void RegionCommandHandle(ISender sender, ArgumentList args)
        {
            var player = sender as BasePlayer;
            if (player == null)
            {
                sender.SendMessage("You must be a player to use this command!", 255, 255, 0, 0);
                return;
            }

            if (args.Count >= 1)
            {
                var arg = "";
                if (args.TryGetString(0, out arg))
                {
                    if (!Regions.ContainsKey(player.Name))
                        Regions.Add(player.Name, new PlayerRegion(player));
                    switch (arg)
                    {
                        case "select":
                            var index = 0;
                            if (args.TryGetInt(1, out index))
                            {
                                if (index - 1 < PlayerRegion.MAX_REGIONS)
                                {
                                    Regions[player.Name].SelectRegion(index - 1);
                                    player.SendMessage(String.Format("Selected Region {0}",
                                        Regions[player.Name].SelectedRegionIndex + 1));
                                }
                            }
                            break;
                        case "set":
                            var pointnum = 0;
                            if (args.TryGetInt(1, out pointnum))
                            {
                                if (Regions[player.Name].SelectedRegionIndex == -1)
                                {
                                    player.SendMessage("Please select a region: /r select <Region Number>", Color.Red);
                                    return;
                                }
                                if (Regions[player.Name].GetSelectedRegion() == null)
                                {
                                    if (pointnum == 1 || pointnum == 2)
                                    {
                                        Regions[player.Name].AddRegion().Config.IsSettingPoint(pointnum - 1);
                                    }
                                }
                                else
                                {
                                    if (pointnum == 1 || pointnum == 2)
                                    {
                                        Regions[player.Name].SetSetting(pointnum - 1);
                                    }
                                }
                                player.SendMessage(string.Format("Seting point: {0} of region: {1}", pointnum,
                                    Regions[player.Name].SelectedRegionIndex + 1));
                            }
                            else
                            {
                                if (Regions[player.Name].GetSelectedRegion() != null)
                                    Regions[player.Name].GetSelectedRegion().SetRect();
                            }
                            break;
                    }
                }

            }
        }

        [Hook(HookOrder.NORMAL)]
        private void OnPlayerWorldChange(ref HookContext ctx, ref HookArgs.PlayerWorldAlteration args)
        {
            if (Regions.ContainsKey(ctx.Player.Name))
                if (Regions[ctx.Player.Name].SelectedRegion != null)
                    if (Regions[ctx.Player.Name].SelectedRegion.Config.IsSetting)
                    {
                        Regions[ctx.Player.Name].SetPoint(new Point(args.X, args.Y));
                        ctx.Player.SendMessage(string.Format("Set Point {0} To: {1}, {2}",
                            Regions[ctx.Player.Name].SelectedRegion.Config.SettingPoint, args.X, args.Y));
                        ctx.SetResult(HookResult.RECTIFY);
                        return;
                    }
            foreach (var keyValuePair in Regions)
            {
                foreach (var region in keyValuePair.Value.Regions)
                {
                    if(region != null)
                        if (region.IsWithinRegion(args.X, args.Y) && keyValuePair.Key != ctx.Player.Name)
                        {
                            //Player is not owner of region 
                            ctx.Sender.SendMessage("Yo, You dont go no perms to do shit here");
                            ctx.SetResult(HookResult.RECTIFY);
                        }
                }
            }

        }
    }
}
