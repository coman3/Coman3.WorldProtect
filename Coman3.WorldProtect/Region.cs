﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using TDSM.API.Logging;

namespace Coman3.WorldProtect
{
    public class Region
    {
        /// <summary>
        /// Maximum Width of a region. Exclusive of value.
        /// </summary>
        public const int MAX_WIDTH = 500;
        /// <summary>
        /// Maximum Height of a region. Exclusive of value.
        /// </summary>
        public const int MAX_HEIGHT = 500;
        /// <summary>
        /// Minimum Width of a region. Exclusive of value.
        /// </summary>
        public const int MIN_WIDTH = 5;
        /// <summary>
        /// Minimum Height of a region. Exclusive of value.
        /// </summary>
        public const int MIN_HEIGHT = 5;

        public Point[] Points { get; set; }
        public Rectangle Rectangle { get; set; }
        public RegionConfig Config { get; set; }

        public string Name { get; set; }
        public bool IsValid => (Rectangle.Width < MAX_WIDTH && Rectangle.Height < MAX_HEIGHT) 
            && (Rectangle.Width > MIN_WIDTH && Rectangle.Height > MIN_HEIGHT);

        public Region(Point point1, Point point2, RegionConfig config)
        {
            Points = new Point[2];
            Config = config;
            SetRect(point1, point2);
        }

        public Region(int x1, int y1, int x2, int y2, RegionConfig config)
            : this(new Point(x1, y1), new Point(x2, y2), config)
        {
        }

        public Region(RegionConfig config)
        {
            Points = new Point[2];
            Config = config;
        }
        public void SetRect()
        {
            SetRect(Points[0], Points[1]);
        }

        public void SetRect(Point point1, Point point2)
        {
            if (Points[0] == Point.Zero || Points[1] == Point.Zero) return;

            Points[0] = point1;
            Points[1] = point2;
            if (point1.X > point2.X)
                if (point1.Y > point2.Y)
                    Rectangle = new Rectangle(point2.X, point2.Y, point1.X - point2.X, point1.Y - point2.Y);
                else
                    Rectangle = new Rectangle(point2.X, point1.Y, point1.X - point2.X, point2.Y - point1.Y);
            else if (point1.Y > point2.Y)
                Rectangle = new Rectangle(point1.X, point2.Y, point2.X - point1.X, point1.Y - point2.Y);
            else
                Rectangle = new Rectangle(point1.X, point1.Y, point2.X - point1.X, point2.Y - point1.Y);
            ProgramLog.Log("Region Set - \n X: {0} \n Y: {1} \n Width: {2} \n Height: {3} \n IsValid: {4}",
                Rectangle.X, Rectangle.Y, Rectangle.Width, Rectangle.Height, IsValid);
        }

        public bool IsWithinRegion(int x, int y)
        {
            return Rectangle.Contains(x, y);
        }
    }

    public class RegionConfig
    {
        public int Index { get; set; }
        public bool IsSetting { get; set; }
        public int SettingPoint { get; set; }

        public RegionConfig(int index, bool isSetting)
        {
            Index = index;
            IsSetting = isSetting;
        }

        public void IsSettingPoint(int pointIndex)
        {
            IsSetting = true;
            SettingPoint = pointIndex;
        }
    }
}
