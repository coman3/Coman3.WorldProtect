﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using TDSM.API;
using TDSM.API.Logging;

namespace Coman3.WorldProtect
{
    public class PlayerRegion
    {
        public const int MAX_REGIONS = 2;

        public int SelectedRegionIndex { get; set; }

        public Region SelectedRegion
        {
            get { return GetSelectedRegion(); }
        }
        public Region[] Regions { get; set; }

        public BasePlayer Player { get; private set; }
        public bool Setting { get; set; }

        public PlayerRegion(BasePlayer player)
        {
            SelectedRegionIndex = -1;
            Player = player;
            Regions = new Region[MAX_REGIONS];
        }

        public Region GetSelectedRegion()
        {
            if (SelectedRegionIndex < Regions.Length)
                return Regions[SelectedRegionIndex];
            return null;
        }
        public Region SelectRegion(int index)
        {
            if (index < Regions.Length)
            {
                SelectedRegionIndex = index;
                return Regions[index];
            }
            return null;
        }
        public Region AddRegion()
        {
            if (SelectedRegionIndex < Regions.Length)
            {
                var region = new Region(new RegionConfig(SelectedRegionIndex, false));
                Regions[SelectedRegionIndex] = region;
                return region;
            }
            ProgramLog.Log("To Many Regions For Player: " + Player.Name);
            return null;
        }

        public void SetSetting(int pointNumber)
        {
            if (SelectedRegionIndex != -1 && GetSelectedRegion() != null)
            {
                Setting = true;
                this.GetSelectedRegion().Config.IsSettingPoint(pointNumber);
            }
        }

        public void SetPoint(Point point)
        {
            if (SelectedRegionIndex != -1 && GetSelectedRegion() != null)
            {
                GetSelectedRegion().Points[GetSelectedRegion().Config.SettingPoint] = point;
                Setting = false;
                this.GetSelectedRegion().Config.IsSetting = false;
            }
        }

    }
}
