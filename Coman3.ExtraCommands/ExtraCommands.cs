﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDSM.API;
using TDSM.API.Command;
using TDSM.API.Logging;
using TDSM.API.Plugin;
using Terraria;

namespace Coman3.ExtraCommands
{
    public class ExtraCommands : BasePlugin
    {
        public Dictionary<string, string> CommandDictionary { get; set; }
        public CommandParser CommandParser { get; set; }
        public ExtraCommands()
        {
            TDSMBuild = 5;
            Version = "1.0";
            Author = "Coman3";
            Name = "Extra Commands";
            Description = "Adds a group of usefull commands";
            CommandDictionary = new Dictionary<string, string>(Tools.MaxPlayers + 1);
            CommandParser = new CommandParser();
        }

        protected override void Initialized(object state)
        {
            #region All Help Command Additions

            AddCommand("allhelp")
                .WithAccessLevel(AccessLevel.PLAYER)
                .WithDescription("Displays all the commands available to the user.")
                .WithPermissionNode("tdsm.help")
                .Calls(AllHelpCommandHandle);
            AddCommand("ahelp")
                .WithAccessLevel(AccessLevel.PLAYER)
                .WithDescription("Displays all the commands available to the user.")
                .WithPermissionNode("tdsm.help")
                .Calls(AllHelpCommandHandle);

            #endregion

            AddCommand("!")
                .WithAccessLevel(AccessLevel.PLAYER)
                .WithDescription("Runs the last command executed by you.")
                .WithPermissionNode("extracommands.previous")
                .Calls(PreviousCommandHandle);
        }

        private void AllHelpCommandHandle(ISender sender, ArgumentList args)
        {
            var commands = sender.GetAvailableCommands();

            if (commands != null && commands.Count > 0)
            {
                var sorted = commands
                    .OrderBy(x => x.Key.ToLower())
                    .Select(x => x.Value)
                    .ToArray();

                var cmds = new List<CommandInfo>();
                cmds.AddRange(sorted);

                var prefixMax = cmds
                    .Select(x => x.Prefix.Length)
                    .OrderByDescending(x => x)
                    .First();

                foreach (var cmd in sorted)
                    cmd.ShowDescription(sender, prefixMax);
            }
            else
                sender.SendMessage("No available commands.");
        }

        private void PreviousCommandHandle(ISender sender, ArgumentList args)
        {
            var player = sender as Player;
            if (player != null)
            {
                if (CommandDictionary.ContainsKey(player.Name))
                    CommandParser.ParsePlayerCommand(player, CommandDictionary[player.Name]);
                else
                sender.SendMessage("No Previous Command", 255, 255, 20, 20);
                //ProgramLog.Log("{0}", ctx.Player.Name); //, args.Prefix + " " + args.ArgumentString);
            }
            if (sender is ConsoleSender)
            {
                if (CommandDictionary.ContainsKey("CONSOLE"))
                    CommandParser.ParseConsoleCommand(CommandDictionary["CONSOLE"]);
                else
                    sender.SendMessage("No Previous Command", 255, 255, 20, 20);
            }
        }

        [Hook(HookOrder.LATE)]
        private void Command(ref HookContext ctx, ref HookArgs.Command args)
        {
            if (args.Prefix == "!") return;

            if (ctx.Player != null)
            {
                if (CommandDictionary.ContainsKey(ctx.Player.Name))
                    CommandDictionary[ctx.Player.Name] = args.Prefix + " " + args.ArgumentString;
                else
                    CommandDictionary.Add(ctx.Player.Name, args.Prefix + " " + args.ArgumentString);
                //ProgramLog.Log("{0}", ctx.Player.Name); //, args.Prefix + " " + args.ArgumentString);
            }
            if (ctx.Sender is ConsoleSender)
            {
                if (CommandDictionary.ContainsKey("CONSOLE"))
                    CommandDictionary["CONSOLE"] = args.Prefix + " " + args.ArgumentString;
                else
                    CommandDictionary.Add("CONSOLE", args.Prefix + " " + args.ArgumentString);
            }
        }
    }
}
