﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Coman3.PluginPatcher
{
    class Program
    {
        private const string TDSM_Path = @"D:\Files\Programing\Terraria\TDSM\Terraria-s-Dedicated-Server-Mod\tdsm-patcher\bin\x86\Debug\";
        private const string TDSM_CORE_PLUGIN_PATH = @"D:\Files\Programing\Terraria\TDSM\Terraria-s-Dedicated-Server-Mod\tdsm-core\bin\x86\Debug\TDSM.Core.dll";


        private const string Plugin_Debug_Path = @"D:\Files\Programing\Terraria\TDSM\Plugins\Coman3.ExtraCommands\Coman3.WorldProtect\bin\Debug";


        static void Main(string[] args)
        {
            try
            {
                foreach (var file in Directory.EnumerateFiles(TDSM_Path))
                {
                    File.Copy(file, Path.Combine(Plugin_Debug_Path, new FileInfo(file).Name), false);
                }

                if (!Directory.Exists(Path.Combine(Plugin_Debug_Path, "Libraries")))
                    Directory.CreateDirectory(Path.Combine(Plugin_Debug_Path, "Libraries"));

                foreach (var file in Directory.EnumerateFiles(Path.Combine(TDSM_Path, "Libraries")))
                {
                    if(!File.Exists(Path.Combine(Plugin_Debug_Path, "Libraries", new FileInfo(file).Name)))
                        File.Copy(file, Path.Combine(Plugin_Debug_Path, "Libraries", new FileInfo(file).Name));
                }

                File.Copy(TDSM_CORE_PLUGIN_PATH, Path.Combine(Plugin_Debug_Path, "Plugins\\TDSM.Core.dll"), true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Running Server...");
            Thread.Sleep(1000);
            Process.Start(Path.Combine(Plugin_Debug_Path, "tdsm.exe"));
        }
    }
}
